<?php

Route::get('jobs/{timezone?}',
  'hyperdrivedesigns\larajobsmanager\LarajobsmanagerController@time');

  Route::get('jobs',
    'hyperdrivedesigns\larajobsmanager\LarajobsmanagerController@index');
