<?php

namespace hyperdrivedesigns\larajobsmanager;

use Illuminate\Support\ServiceProvider;

class LarajobsmanagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'larajobsmanager');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('hyperdrivedesigns\larajobsmanager\LarajobsmanagerController');
    }
}
