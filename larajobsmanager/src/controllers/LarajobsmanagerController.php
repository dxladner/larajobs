<?php

namespace hyperdrivedesigns\larajobsmanager;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Larajob;

class LarajobsmanagerController extends Controller
{
    public function time($timezone = NULL)
    {
        $current_time = ($timezone)
            ? Carbon::now(str_replace('-', '/', $timezone))
            : Carbon::now();
        return view('larajobsmanager::site.index', compact('current_time'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $larajobs= Larajob::all();
      return $larajobs;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $larajob = Larajob::findOrFail($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $larajob = Larajob::findOrFail($id);
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $larajob = Larajob::findOrFail($id);
        $larajob->name = $request->get('name');
        $larajob->email = $request->get('email');
        $larajob->save();
        return redirect(action('UserController@edit', $user->id))->with('status', 'The user has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $larajob = Larajob::findOrFail($id);
        $larajob->delete();
        return redirect('/users')->with('status', 'The user has been deleted!');
    }

}
