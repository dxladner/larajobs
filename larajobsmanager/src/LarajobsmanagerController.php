<?php

namespace hyperdrivedesigns\larajobsmanager;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Larajob;

class LarajobsmanagerController extends Controller
{
    public function time($timezone = NULL)
    {
        $current_time = ($timezone)
            ? Carbon::now(str_replace('-', '/', $timezone))
            : Carbon::now();
        return view('larajobsmanager::site.index', compact('current_time'));
    }

    public function index()
    {
      $larajobs= Larajob::all();
      return $larajobs;
    }

}
